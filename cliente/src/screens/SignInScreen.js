import { View, Text, Image, StyleSheet, ScrollView, Alert, TouchableOpacity, StatusBar } from 'react-native';
import React, { useState } from 'react';
import axios from 'axios';

import CustomInput from '../components/CustomInput';
import CustomButton from '../components/CustomButton';

const SignInScreen = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const onSignInPressed = () => {
        if (email.length === 0 || password.length === 0) {
            return (Alert.alert(
                "Error",
                "All fields must be provided",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        axios.post('http://192.168.100.2:5000/user/login', {
            email: email,
            password: password
        })
            .then((res) => {
                console.log(res.data.data.id);
                navigation.navigate('Home', {
                    paramKey: res.data.data.id,
                });
            })
            .catch((err) => {
                Alert.alert(
                    "There was an error",
                    "Verify your data and try logging in again",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })
    }

    const onForgotPasswordPressed = () => {
        if (email.length === 0) {
            return (Alert.alert(
                "Error",
                "Email must be provided",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        axios.post(`http://192.168.100.2:5000/email/${email}`)
            .then((res) => {
                Alert.alert(
                    "The email has been sent with sucess",
                    "Check your email to restore your password",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })
            .catch((err) => {
                console.log(err);
                Alert.alert(
                    "There was an error",
                    "Verify your data and try again",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })
    }

    return (
        <ScrollView showsHorizontalScrollIndicator={false} >
            <View style={styles.root}>
                <StatusBar animated={true} backgroundColor='#2B4257' />
                <Image source={require('../../assets/images/PASSafeLogo.png')} />
                <Text style={styles.text}>Login</Text>
                <Text style={styles.text}>Please, fill the fields with your information: </Text>
                <CustomInput placeholder='E-mail' value={email} setValue={setEmail} />
                <CustomInput placeholder='Password' value={password} setValue={setPassword} secureTextEntry />

                <CustomButton text='Log in' onPress={onSignInPressed} />
                <CustomButton text='Forgot your password? fill with your email' onPress={() => onForgotPasswordPressed()} type='TERTIARY' />
                <CustomButton text='You dont have an account? Press here' onPress={() => navigation.navigate("Signup")} type='TERTIARY' />
                <TouchableOpacity style={styles.utilsContainer} onPress={() => navigation.navigate("FreqAsked")}><Text style={styles.utils}>Frequently asked questions</Text></TouchableOpacity>
                <TouchableOpacity style={styles.utilsContainer} onPress={() => navigation.navigate("TermsOfService")}><Text style={styles.utils}>By registering you agree to the terms of service</Text></TouchableOpacity>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#F2EBE9'
    },
    text: {
        padding: 8,
        fontWeight: 'bold',
    },
    utilsContainer: {
        width: '100%',
        padding: 6,
        marginVertical: 5,
        alignItems: 'center',
        borderRadius: 5,
    },
    utils: {
        color: 'grey',
        fontWeight: 'bold'
    }
})

export default SignInScreen;