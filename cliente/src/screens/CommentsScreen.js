import { StyleSheet, Text, View, SafeAreaView, FlatList, ActivityIndicator, TextInput, TouchableOpacity, StatusBar, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { Divider } from '@rneui/base';

const FullPost = ({ title, description, author }) => {
    return (
        <View style={styles.post}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.description}>{description}</Text>
            <Text style={styles.title}>Comments: </Text>
        </View>
    )
}

const Comment = ({ description }) => {
    return (
        <View style={styles.comment}>
            <Divider style={styles.divider} />
            <Text style={styles.description}>{description}</Text>
        </View>
    )
}

const CommentsScreen = ({ route }) => {
    const [commentId, setCommentId] = useState("");
    const [userId, setUserId] = useState("");
    const [comments, setComments] = useState([]);
    const [newComment, setNewComment] = useState("");
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [author, setAuthor] = useState("");
    const [isLoading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(false);

    const comment = () => {
        let postComment = {
            userId: route.params.paramKey.id,
            comment: newComment,
        }
        comments.push(postComment);

        axios.patch(`http://192.168.100.2:5000/querie/${route.params.paramKey.commentsId}`, {
            _id: route.params.paramKey.commentsId,
            userId: userId,
            title: title,
            description: description,
            comments: comments
        })
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setNewComment("");
                setRefresh(!refresh);
            })
    }

    useEffect(() => {
        setCommentId(route.params.paramKey.commentsId);

        axios.get(`http://192.168.100.2:5000/querie/${route.params.paramKey.commentsId}`)
            .then((res) => {
                setTitle(res.data.data.querie.title);
                setDescription(res.data.data.querie.description);
                setAuthor(res.data.data.querie.author);
                setComments(res.data.data.querie.comments);
                setUserId(res.data.data.querie.userId);
                console.log(res.data.data.querie.comments)
                console.log(comments);

            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
                setRefresh(!refresh);
            })
    }, [])

    const Header = () => {
        return (
            <View>
                <StatusBar animated={true} backgroundColor='#2B4257' />
                <FullPost title={title} description={description} />
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.general}>

            {isLoading ? <ActivityIndicator /> :
                <><FlatList
                    style={styles.flatlistComments}
                    data={comments}
                    keyExtractor={(item) => item._id}
                    renderItem={({ item }) => <Comment description={item.comment} />}
                    ListHeaderComponent={Header}
                    refreshing={refresh} /><View style={styles.commentContainer}>
                        <TextInput numberOfLines={3} multiline={true} placeholderTextColor='white' style={styles.input} placeholder="Comment here!" value={newComment} onChangeText={(e) => setNewComment(e)} />
                        <TouchableOpacity style={styles.commentButton} text="Post comment" onPress={() => comment()}>
                            <Text style={styles.commentButtonText}>Post comment</Text>
                        </TouchableOpacity>
                    </View></>


            }
        </SafeAreaView>
    )
}


export default CommentsScreen

const styles = StyleSheet.create({
    general: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: '#F2EBE9'
    },
    post: {
        backgroundColor: '#14202E',
        borderRadius: 15,
        padding: 5,
        marginVertical: 8,
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 17,
    },
    description: {
        color: 'white',
        fontSize: 13,
    },
    author: {
        color: 'white',
    },
    comment: {
        paddingHorizontal: 5,
        marginVertical: 5,
    },
    input: {
        backgroundColor: 'black',
        borderRadius: 15,
        color: 'white',
        width: '80%',
        marginRight: 5,
        paddingLeft: 10,
    },
    commentButton: {
        backgroundColor: 'black',
        borderRadius: 15,
        width: '20%',
    },
    commentButtonText: {
        color: 'white',
        textAlign: 'center',
        height: 30,
        paddingTop: 13,
    },
    commentContainer: {
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 50,
    },
    flatlistComments: {
        backgroundColor: '#14202E',
        borderRadius: 15,
        padding: 5,
        paddingBottom: 10,
    },
    divider: {
        height: 1,
        margin: 3,
        backgroundColor: 'white',
    }
})