import { StyleSheet, Text, StatusBar, ScrollView, Button, ActivityIndicator, Alert, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { LineChart, BarChart, PieChart } from 'react-native-chart-kit'
import { ButtonGroup } from '@rneui/base';

const StatsScreen = () => {
    const longPasswordCounter = useSelector((state) => state.stats.LongPasswordGenerated);
    const shortPasswordCounter = useSelector(state => state.stats.ShortPasswordGenerated);
    const pinPasswordCounter = useSelector(state => state.stats.PinPasswordGenerated);
    const generatedPasswords = useSelector(state => state.stats.GeneratedPassword);
    const copiedPasswords = useSelector(state => state.stats.CopiedPassword);
    const [copiedLabels, setCopiedLabels] = useState(["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]);
    const [isLoading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(false);
    const [dataCopied, setDataCopied] = useState([]);
    const [dataCopiedFormated, setDataCopiedFormated] = useState([])
    const [dataGenerated, setDataGenerated] = useState([]);
    const [firstTime, setFirstTime] = useState(true);

    const refreshGraphs = () => {
        setRefresh(!refresh);
    }
    const updateGeneratedStats = () => {
        let day = 0;
        let week = 0;
        let month = 0;
        let year = 0;
        let lastYear = 0;
        let total = 0;
        generatedPasswords.forEach(element => {
            total++
            if (new Date(element).getMonth() + 1 === new Date(Date.now()).getMonth() + 1 && new Date(element).getDate() === new Date(Date.now()).getDate() && new Date(element).getFullYear() === new Date(Date.now()).getFullYear()) {
                day++
            }
            if (new Date(element).getMonth() + 1 === new Date(Date.now()).getMonth() + 1 && new Date(element).getDate() <= new Date(Date.now()).getDate() && new Date(element).getDate() >= new Date(Date.now()).getDate() - 7) {
                week++
            }
            if (new Date(element).getMonth() + 1 === new Date(Date.now()).getMonth() + 1 && new Date(element).getFullYear() === new Date(Date.now()).getFullYear()) {
                month++
            }
            if (new Date(element).getFullYear() === new Date(Date.now()).getFullYear()) {
                year++
            }
            if (new Date(element).getFullYear() === new Date(Date.now()).getFullYear() - 1) {
                lastYear++
            }
        });
        let data = {
            day: day,
            week: week,
            month: month,
            year: year,
            lastYear: lastYear,
            total: total
        }
        setDataGenerated(data);
    }

    const updateCopiedStats = () => {
        let january = 0;
        let february = 0;
        let march = 0;
        let april = 0;
        let may = 0;
        let june = 0;
        let july = 0;
        let august = 0;
        let september = 0;
        let october = 0;
        let november = 0;
        let december = 0;
        let total = 0
        copiedPasswords.forEach(element => {
            total++
            if (new Date(element).getYear() === new Date(Date.now()).getYear()) {
                if (new Date(element).getMonth() + 1 === 1) {
                    january++;
                } else if (new Date(element).getMonth() + 1 === 2) {
                    february++;
                } else if (new Date(element).getMonth() + 1 === 3) {
                    march++;
                } else if (new Date(element).getMonth() + 1 === 4) {
                    april++;
                } else if (new Date(element).getMonth() + 1 === 5) {
                    may++;
                } else if (new Date(element).getMonth() + 1 === 6) {
                    june++;
                } else if (new Date(element).getMonth() + 1 === 7) {
                    july++;
                } else if (new Date(element).getMonth() + 1 === 8) {
                    august++;
                } else if (new Date(element).getMonth() + 1 === 9) {
                    september++;
                } else if (new Date(element).getMonth() + 1 === 10) {
                    october++;
                } else if (new Date(element).getMonth() + 1 === 11) {
                    november++;
                } else if (new Date(element).getMonth() + 1 === 12) {
                    december++;
                }
            }

        })
        let data = {
            january,
            february,
            march,
            april,
            may,
            june,
            july,
            august,
            september,
            october,
            november,
            december,
            total
        }
        setDataCopied(data);
        if (firstTime) {
            setDataCopiedFormated([data.january, data.february, data.march, data.april, data.may, data.june, data.july, data.august, data.september, data.october, data.november, data.december])
            setFirstTime(false);
        }
    }
    const updateCopiedFormatedStats = () => {
        updateCopiedStats();
        setCopiedLabels(["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]);
        setDataCopiedFormated([dataCopied.january, dataCopied.february, dataCopied.march, dataCopied.april, dataCopied.may, dataCopied.june, dataCopied.july, dataCopied.august, dataCopied.september, dataCopied.october, dataCopied.november, dataCopied.december]);
    }


    const January = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Jan");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Jan");
            dataCopiedFormated.splice(0, 0, dataCopied.january);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Jan</Text></TouchableOpacity>
    const February = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Feb");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Feb");
            dataCopiedFormated.splice(0, 0, dataCopied.february);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Feb</Text></TouchableOpacity>
    const March = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Mar");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Mar");
            dataCopiedFormated.splice(0, 0, dataCopied.march);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Mar</Text></TouchableOpacity>
    const April = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Apr");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Apr");
            dataCopiedFormated.splice(0, 0, dataCopied.april);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Apr</Text></TouchableOpacity>
    const May = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("May");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "May");
            dataCopiedFormated.splice(0, 0, dataCopied.may);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>May</Text></TouchableOpacity>
    const June = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Jun");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Jun");
            dataCopiedFormated.splice(0, 0, dataCopied.june);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Jun</Text></TouchableOpacity>

    const July = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Jul");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Jul");
            dataCopiedFormated.splice(0, 0, dataCopied.july);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Jul</Text></TouchableOpacity>
    const August = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Aug");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Aug");
            dataCopiedFormated.splice(0, 0, dataCopied.august);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Aug</Text></TouchableOpacity>
    const September = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Sep");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Sep");
            dataCopiedFormated.splice(0, 0, dataCopied.september);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Sep</Text></TouchableOpacity>
    const October = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Oct");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Oct");
            dataCopiedFormated.splice(0, 0, dataCopied.october);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Oct</Text></TouchableOpacity>
    const November = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Nov");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Nov");
            dataCopiedFormated.splice(0, 0, dataCopied.november);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Nov</Text></TouchableOpacity>
    const December = () => <TouchableOpacity onPress={() => {
        const index = copiedLabels.indexOf("Dec");
        if (index != -1) {
            copiedLabels.splice(index, 1);
            dataCopiedFormated.splice(index, 1);
        } else if (index === -1) {
            copiedLabels.splice(0, 0, "Dec");
            dataCopiedFormated.splice(0, 0, dataCopied.december);
        }
        setRefresh(!refresh);
    }} style={styles.monthsButtons}><Text style={styles.monthsButtonsText}>Dec</Text></TouchableOpacity>

    const Months = [{ element: January }, { element: February }, { element: March }, { element: April }, { element: May }, { element: June }]
    const Months2 = [{ element: July }, { element: August }, { element: September }, { element: October }, { element: November }, { element: December }]

    const updateStats = () => {
        updateGeneratedStats();
        updateCopiedStats();
        setLoading(false);
    }

    useEffect(() => {
        if (!refresh || refresh) {
            updateStats();
        }
    }, [refresh])
    return (

        <ScrollView contentContainerStyle={styles.general} >

            <StatusBar backgroundColor='#2B4257' />
            <Text style={styles.screenTitle} >Statistics</Text>
            {/* Comparacion entre distintos tipos de password */}
            <Text style={styles.title} >Comparison between the different types of passwords created</Text>
            <PieChart
                data={[
                    {
                        name: "Long",
                        quantity: longPasswordCounter,
                        color: "#189AB4",
                        legendFontColor: 'black',
                        legendFontSize: 13
                    },
                    {
                        name: "Short",
                        quantity: shortPasswordCounter,
                        color: "#145DA0",
                        legendFontColor: 'black',
                        legendFontSize: 13
                    },
                    {
                        name: "PIN",
                        quantity: pinPasswordCounter,
                        color: "#0C2D48",
                        legendFontColor: 'black',
                        legendFontSize: 13
                    }
                ]}
                width={380}
                height={220}
                chartConfig={{
                    backgroundColor: "#091235",
                    backgroundGradientFrom: "#14202E",
                    backgroundGradientTo: "#2B4257",
                    decimalPlaces: 0,
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                }}
                accessor={"quantity"}
                backgroundColor={"transparent"}
                paddingLeft={"15"}
                absolute
            />
            {/* Comparacion de uso entre dia, semana y mes */}
            <Text style={styles.title} >Comparison between the number of uses each month in the current year and in total</Text>
            <ButtonGroup buttons={Months} containerStyle={styles.buttonGroupContainer} />
            <ButtonGroup buttons={Months2} containerStyle={styles.buttonGroupContainer} />
            {isLoading ? <ActivityIndicator /> : <BarChart
                data={{
                    labels: copiedLabels,
                    datasets: [
                        {
                            data: dataCopiedFormated
                        }
                    ]
                }}
                width={400}
                height={220}
                fromZero={true}
                showValuesOnTopOfBars={true}
                chartConfig={{
                    backgroundColor: "#091235",
                    backgroundGradientFrom: "#14202E",
                    backgroundGradientTo: "#2B4257",
                    decimalPlaces: 0,
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    strokeWidth: 2,
                    barPercentage: 0.3,


                }}
                style={{
                    borderRadius: 16,
                }}
            />
            }
            <Text style={styles.total}>Total: {dataCopied.total}</Text>
            <TouchableOpacity onPress={() => {
                updateCopiedFormatedStats();
            }} style={styles.refreshButton} ><Text style={styles.refreshText}>Refresh Graph</Text></TouchableOpacity>
            {/* Comparacion de creacion entre dia semana y mes */}
            <Text style={styles.title} >Comparison between the number of passwords created per day, week, month and year, last year and total</Text>
            {isLoading ? <ActivityIndicator /> :
                <LineChart
                    data={{
                        labels: ["Day", "Week", "Month", "Year", "Last Year", "Total"],
                        datasets: [
                            {
                                data: [
                                    dataGenerated.day,
                                    dataGenerated.week,
                                    dataGenerated.month,
                                    dataGenerated.year,
                                    dataGenerated.lastYear,
                                    dataGenerated.total
                                ]
                            }
                        ]
                    }}
                    width={380}
                    height={220}
                    fromZero={true}
                    yAxisInterval={1}
                    chartConfig={{
                        backgroundColor: "#091235",
                        backgroundGradientFrom: "#14202E",
                        backgroundGradientTo: "#2B4257",
                        decimalPlaces: 0,
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 16,
                            padding: 20,
                            margin: 20,
                        },
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#2B4257"
                        }
                    }}
                    style={{
                        marginVertical: 5,
                        borderRadius: 16
                    }}
                />
            }

            <TouchableOpacity onPress={() => {
                updateStats();
            }} style={styles.refreshButton} ><Text style={styles.refreshText}>Refresh Graph</Text></TouchableOpacity>
        </ScrollView>
    )
}

export default StatsScreen

const styles = StyleSheet.create({
    general: {
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#F2EBE9'
    },
    screenTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#2F5061',

    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#2F5061',
        textAlign: 'center',
    },
    refreshButton: {
        backgroundColor: 'black',
        height: 30,
        width: '50%',
        borderRadius: 16,
        margin: 4,
    },
    refreshText: {
        paddingTop: 5,
        color: 'white',
        textAlign: 'center',
    },
    monthsButtons: {
        width: '100%',
        height: '100%',
        padding: 7,
        alignItems: 'center',
    },
    monthsButtonsText: {
        color: 'white',
    },
    buttonGroupContainer: {
        backgroundColor: '#2B4257',
        borderRadius: 20,
        height: 40,
    },
    total: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#2F5061',
        textAlign: 'center',
    }
})