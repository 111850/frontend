import { StyleSheet, Text, View, StatusBar, TextInput, TouchableOpacity, FlatList, SafeAreaView, Alert } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import Clipboard from '@react-native-clipboard/clipboard';
import CustomInput from '../components/CustomInput';
import CustomButton from '../components/CustomButton';

import { useDispatch, useSelector } from 'react-redux';
import { addCounterLongPassword, addCounterShortPassword, addCounterPinPassword, newPasswordGenerated, newPasswordCopied, addTests } from '../../redux/statsSlice'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ButtonGroup, Divider } from '@rneui/themed';

const PasswordsScreen = ({ route }) => {
    const [Id, setId] = useState(route.params.Id);
    const [isLoading, setLoading] = useState(true);
    const [newKeyword, setKeyword] = useState("");
    const [passwordGen, setPasswordGen] = useState("");
    const [passwordLength, setPasswordLength] = useState("14");
    const [chars, setChars] = useState("0123456789abcdefghijklmnopqrstuvwxyz!@$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    const [pin, setPin] = useState("");
    const [isLoggedWithPin, setLoggedWithPin] = useState(false);
    // const numbers = "1234567890";
    // const upperCaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // const lowerCaseCharacters = "abcdefghijklmnopqrstuvwxyz";
    // const specialCharacters = "!@#$%^&*()";
    const [passwords, setPasswords] = useState([]);
    const dispatch = useDispatch();
    const listCopiedPasswords = useSelector(state => state.stats.CopiedPassword);
    const listPasswords = useSelector((state) => state.stats.GeneratedPassword);
    const longPasswordCounter = useSelector((state) => state.stats.LongPasswordGenerated);
    const shortPasswordCounter = useSelector(state => state.stats.ShortPasswordGenerated);
    const pinPasswordCounter = useSelector(state => state.stats.PinPasswordGenerated);
    const [refresh, setRefresh] = useState(false);

    const insertPin = () => {
        if (pin.length === 0 || isNaN(parseInt(pin))) {
            return (Alert.alert(
                "Error",
                "You either leave the field empty or insert a character instead of a number",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        axios.post('http://192.168.100.2:5000/user/pin', {
            userId: Id,
            pin: pin
        })
            .then((res) => {
                setLoggedWithPin(true);
            })
            .catch((err) => {
                Alert.alert(
                    "Error",
                    "There was an error trying to verify your pin",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })

    }

    const updateList = () => {
        axios.get(`http://192.168.100.2:5000/password/${Id}`)
            .then((res) => {
                setPasswords(res.data.data.decryptedPasswords);
                console.log(res.data.data.decryptedPasswords);
                // console.log(res.data.data.passwords)
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            })
    }

    const createPassword = (keyword, genPassword, userId) => {
        if (keyword.length === 0) {
            return (Alert.alert(
                "Error",
                "Keyword value must not be empty",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        axios.post('http://192.168.100.2:5000/password', {
            keyword: keyword,
            genPassword: genPassword,
            userId: userId
        })
            .then((res) => {
                AsyncStorage.setItem("@GeneratedPasswords", JSON.stringify([...listPasswords, new Date(Date.now())]));
                dispatch(newPasswordGenerated());
                if (genPassword.length === 14) {
                    AsyncStorage.setItem("@LongPasswordCounter", JSON.stringify(longPasswordCounter + 1));
                    dispatch(addCounterLongPassword());
                } else if (genPassword.length === 8) {
                    AsyncStorage.setItem("@ShortPasswordCounter", JSON.stringify(shortPasswordCounter + 1));
                    dispatch(addCounterShortPassword());
                } else {
                    AsyncStorage.setItem("@PinPasswordCounter", JSON.stringify(pinPasswordCounter + 1));
                    dispatch(addCounterPinPassword());
                }
                updateList();
                Alert.alert(
                    "Password created successfully",
                    "Password created successfully",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })
            .catch((err) => {
                Alert.alert(
                    "There was an error",
                    "Verify your data and try again",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })
    }
    const deletePassword = (id) => {
        axios.delete(`http://192.168.100.2:5000/password/${id}`)
            .then((res) => {
                updateList();
            })
            .catch((err) => {
                Alert.alert(
                    "Error",
                    "There was an error trying to delete the password",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            });
    }

    const copyPassword = (passwordGen) => {
        Clipboard.setString(passwordGen);
        AsyncStorage.setItem("@CopiedPasswords", JSON.stringify([...listCopiedPasswords, new Date(Date.now())]));
        dispatch(newPasswordCopied());
    }

    const PasswordItem = ({ keyword, passwordGen, id }) => {
        return (

            <View style={styles.password}>
                <Text style={styles.keyword}>{keyword}</Text>
                <Divider orientation="vertical" style={{ backgroundColor: 'white', width: 1 }} />
                <TouchableOpacity style={styles.pressPasswordGen} onPress={() => copyPassword(passwordGen)}>
                    <Text style={styles.passwordGen}> {passwordGen} </Text>
                </TouchableOpacity>
                <Divider orientation="vertical" style={{ backgroundColor: 'white', width: 1, }} />
                <TouchableOpacity onPress={() => deletePassword(id)} style={styles.delete} ><Text style={styles.deleteText}>Delete</Text></TouchableOpacity>
            </View>


        )
    }

    useEffect(() => {
        updateList();
        if (refresh || !refresh) {
            setPasswordGen(generatePassword());
        }
    }, [refresh])

    const LongSettings = () => {
        setChars("0123456789abcdefghijklmnopqrstuvwxyz!@$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        setPasswordLength(14);
        setRefresh(!refresh)

    }
    const Long = () => <TouchableOpacity style={styles.groupBoxButtons} onPress={() => {
        LongSettings();

    }}><Text style={styles.buttonGroupText}>Long</Text></TouchableOpacity>

    const ShortSettings = () => {
        setChars("0123456789abcdefghijklmnopqrstuvwxyz!@$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        setPasswordLength(8);
        setRefresh(!refresh)

    }
    const Short = () => <TouchableOpacity style={styles.groupBoxButtons} onPress={() => {
        ShortSettings();
    }}><Text style={styles.buttonGroupText}>Short</Text></TouchableOpacity>

    const PinSettings = () => {
        setChars("0123456789");
        setPasswordLength(4);
        setRefresh(!refresh)

    }
    const Pin = () => <TouchableOpacity style={styles.groupBoxButtons} onPress={() => {
        PinSettings();
    }}><Text style={styles.buttonGroupText}>PIN</Text></TouchableOpacity>


    const GroupBoxButtons = [{ element: Long }, { element: Short }, { element: Pin }]

    const generatePassword = () => {
        let password = ''
        const characterListLength = chars.length

        for (let i = 0; i < passwordLength; i++) {
            const characterIndex = Math.floor(Math.random() * characterListLength)
            password = password + chars.charAt(characterIndex)
        }
        return password
    }
    return (
        <SafeAreaView style={styles.general}>
            <StatusBar animated={true} backgroundColor='#2B4257' />
            {!isLoggedWithPin ?
                <View style={styles.pinView}>
                    <TextInput style={styles.pinInput} placeholder='PIN' keyboardType='numeric' value={pin} onChangeText={(e) => setPin(e)} autoFocus={true} secureTextEntry />
                    <CustomButton text='Confirm PIN' onPress={() => insertPin()} />
                </View>
                :
                <><View style={styles.createView}>
                    <TextInput placeholderTextColor='white' style={styles.inputKeyword} value={newKeyword} onChangeText={(e) => {
                        setKeyword(e);
                        setPasswordGen(generatePassword());
                    }} placeholder="Insert a Keyword" />
                    <Text style={styles.outputPass}>{passwordGen}</Text>
                    <ButtonGroup buttons={GroupBoxButtons} containerStyle={styles.buttonGroupContainer} />
                    <TouchableOpacity onPress={() => createPassword(newKeyword, passwordGen, route.params.Id)} style={styles.createPassword}><Text style={styles.createPasswordText}>Create Password</Text></TouchableOpacity>
                </View><FlatList
                        data={passwords}
                        keyExtractor={(item) => item._id}
                        renderItem={({ item }) => <PasswordItem keyword={item.keyword} passwordGen={item.genPassword} id={item._id} />}
                        refreshing={refresh}
                    /></>}
        </SafeAreaView>
    )
}

export default PasswordsScreen

const styles = StyleSheet.create({
    general: {
        backgroundColor: '#F2EBE9',
        paddingBottom: 170,
    },
    keyword: {
        paddingTop: 15,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15,
        textAlign: 'left',
        paddingLeft: 25,
        width: '32%',

    },
    passwordGen: {
        paddingTop: 15,
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
        flex: 2,
    },
    delete: {
        paddingHorizontal: 40,
        width: '31%',
    },
    password: {
        margin: 8,
        borderRadius: 18,
        backgroundColor: '#14202E',
        flexDirection: 'row',
        height: 50
    },
    deleteText: {
        paddingTop: 15,
        color: '#E57F84',
        fontSize: 15,
        textAlign: 'right',
    },
    createView: {
        height: 155,
        margin: 8,
        borderRadius: 18,
        backgroundColor: '#14202E',

    },
    inputKeyword: {
        color: 'white',
        textAlign: 'center',
        fontSize: 16,
        padding: 4,
        borderRadius: 20,
        backgroundColor: '#406C83',
        marginTop: 10,
        marginHorizontal: 10,

    },
    outputPass: {
        color: 'white',
        textAlign: 'center',
        fontSize: 16,
        padding: 2,
    },
    createPassword: {
        backgroundColor: '#406C83',
        borderRadius: 20,
        marginHorizontal: 10,
        marginTop: 3,
    },
    createPasswordText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
    },
    buttonGroup: {
        color: 'white',
    },
    buttonGroupContainer: {
        borderRadius: 20,
        height: 35,
        backgroundColor: '#406C83',
    },
    buttonGroupText: {
        color: 'white',
    },
    groupBoxButtons: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        paddingTop: 7,
    },
    pinView: {
        backgroundColor: '#F4EAE6',
        height: '100%',
        width: '30%',
        alignSelf: 'center',
        marginTop: '55%',
    },
    pressPasswordGen: {
        width: '39%',
    },
    pinInput: {
        backgroundColor: 'white',
        width: '100%',
        borderColor: '#e8e8e8',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        marginVertical: 5,
    }
})