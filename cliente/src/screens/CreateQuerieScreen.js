import { StyleSheet, Text, View, Alert, TouchableOpacity, TextInput, StatusBar, ScrollView } from 'react-native'
import React, { useState } from 'react'
import axios from 'axios';

const CreateQuerieScreen = ({ navigation, route }) => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");

    const createQuerie = () => {

        if (title.length === 0 || description.length === 0) {
            return (Alert.alert(
                "Error",
                "Must put a title and description",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        axios.post('http://192.168.100.2:5000/querie', {
            userId: route.params.paramKey.id,
            title: title,
            description: description
        })
            .then((res) => {
                console.log(res);
                Alert.alert(
                    "Querie created successfully",
                    "Querie created successfully",
                    [
                        {
                            text: "Cancel",
                            onPress: () => navigation.navigate('Queries'),
                            style: "cancel"
                        },
                    ],
                );
            })
            .catch((err) => {
                console.log(err);
            })

    }

    return (
        <ScrollView style={styles.general}>
            <StatusBar animated={true} backgroundColor='#2B4257' />
            <Text style={styles.screenTitle}>Create a Querie</Text>
            <TextInput style={styles.input} placeholderTextColor='white' placeholder='Title' onChangeText={(e) => setTitle(e)} value={title} />
            <TextInput style={styles.input} multiline={true} numberOfLines={25} placeholderTextColor='white' placeholder='Description' onChangeText={(e) => setDescription(e)} value={description} />
            <TouchableOpacity style={styles.createQuerie} onPress={() => createQuerie()} ><Text style={styles.createQuerieText}>Create Querie</Text></TouchableOpacity>
        </ScrollView>
    )
}

export default CreateQuerieScreen

const styles = StyleSheet.create({
    general: {
        padding: 10,
        backgroundColor: '#F2EBE9'
    },
    screenTitle: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#2F5061',
        marginBottom: 10,
    },
    input: {
        borderWidth: 2,
        borderRadius: 15,
        backgroundColor: '#14202E',
        paddingHorizontal: 10,
        marginBottom: 10,
        color: 'white',
        textAlignVertical: 'top',
    },
    createQuerie: {
        backgroundColor: 'black',
        borderRadius: 15,
        width: '100%',
        height: 50,
    },
    createQuerieText: {
        color: 'white',
        textAlign: 'center',
        paddingTop: 13,
        fontSize: 15,
    }
})