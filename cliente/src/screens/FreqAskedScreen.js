import { StyleSheet, Text, ScrollView, TouchableOpacity, StatusBar } from 'react-native'
import { Divider } from '@rneui/base'
import React from 'react'

const FreqAskedScreen = ({ navigation }) => {
    return (
        <ScrollView style={styles.general}>
            <StatusBar animated={true} backgroundColor='#2B4257' />
            <Text style={styles.title}>Frequently Asked Questions</Text>
            <Text style={styles.question}>¿Al momento de formatear o cambiar de celular voy a mantener mis contraseñas?</Text>
            <Text style={styles.answer}>{'\t'}Si, las mismas se mantienen en una base de datos y aunque esto pueda parecer inseguro, todas sus contraseñas están encriptadas para asegurar la seguridad el usuario y evitar perdida de datos.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿De dónde salen las noticias que se encuentran en la pantalla de noticias?</Text>
            <Text style={styles.answer}>{'\t'}Las mismas son proporcionadas a través de una API publica llamada NewsAPI que permite hasta 50 extracción de noticias cada 12 horas.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿Si utilizo la misma palabra clave al generar una contraseña, se genera la misma contraseña?</Text>
            <Text style={styles.answer}>{'\t'}No, estas siempre serán distintas ya que se genera una nueva contraseña cada vez que ingrese un carácter en el input.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿Qué beneficios trae un password manager?</Text>
            <Text style={styles.answer}>{'\t'}Los password managers aseguran tener las contraseñas al alcance y eliminar la necesidad de recordar las contraseñas de todas nuestras cuentas, aunque siempre garantizando la seguridad de las mismas.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿Qué tan seguro es tener mis contraseñas en un password manager?</Text>
            <Text style={styles.answer}>{'\t'}La seguridad de las contraseñas depende de dos factores: La variación y longitud de la misma y la probabilidad de que se pierdan datos de nuestra base de datos. Nuestro producto garantiza que las contraseñas generadas y utilizadas son mucho mas seguras que cualquier contraseña que puede pensar un humano.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿Qué plataformas soporta esta aplicación?</Text>
            <Text style={styles.answer}>{'\t'}Nuestra aplicación funciona solamente en dispositivos Android y IOS.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿Como creo mi cuenta?</Text>
            <Text style={styles.answer}>{'\t'}Para crear su cuenta, es necesario rellenar los campos en la pantalla de registro: Usuario, Email, Contraseña, Confirmar Contraseña y PIN.</Text>
            <Divider style={styles.divider} />
            <Text style={styles.question}>¿A qué dirección de mail me puedo contactar para hacer una sugerencia o reclamo?</Text>
            <Text style={styles.answer}>{'\t'}En caso de tener algún problema con la aplicación puede contactarnos a passafeee@gmail.com</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Signin')} style={styles.goBackButton}><Text style={styles.goBack}>Go Back</Text></TouchableOpacity>
        </ScrollView>
    )
}

export default FreqAskedScreen;

const styles = StyleSheet.create({
    question: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    answer: {
        fontSize: 14,
        textAlign: 'justify',
    },
    general: {
        backgroundColor: '#F2EBE9',
        paddingHorizontal: 10,
    },
    title: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        marginVertical: 7,
    },
    goBackButton: {
        marginVertical: 8,
        paddingTop: 5,
        width: '75%',
        height: 30,
        backgroundColor: '#4297A0',
        borderRadius: 15,
        alignSelf: 'center',
    },
    goBack: {
        fontWeight: 'bold',
        fontSize: 17,
        textAlign: 'center',
        color: '#F2EBE9',
    },
    divider: {
        margin: 10,
        backgroundColor: 'black',
        height: 1,
    }
})