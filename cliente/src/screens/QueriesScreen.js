import { View, Text, ScrollView, StyleSheet, StatusBar, ActivityIndicator, FlatList, SafeAreaView, TouchableOpacity, Alert } from 'react-native'
import React, { useEffect, useState } from 'react'
import { Divider } from '@rneui/base';
import axios from 'axios';

const Post = ({ title, description, onPress }) => {
    return (
        <View style={styles.post}>
            <Text style={styles.postTitle}>{title}</Text>
            <Divider style={styles.divider} />
            <Text style={styles.postDescription}>{description}</Text>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={onPress} style={styles.comments}><Text style={styles.commentsText}>See Comments</Text></TouchableOpacity>
                <TouchableOpacity onPress={onPress} style={styles.reply}><Text style={styles.commentsText}>Reply</Text></TouchableOpacity>
            </View>
        </View>
    )
}

const QueriesScreen = ({ navigation, route }) => {
    const [queries, setQueries] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(false);

    const seeComments = (commentsId) => {
        navigation.navigate('Comments', {
            paramKey: {
                commentsId: commentsId,
                id: route.params.Id
            }
        });
    }

    const updateQueries = () => {
        axios.get('http://192.168.100.2:5000/querie')
            .then((res) => {
                setQueries(res.data.data.queries);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            })
    }

    const createQuerie = () => {
        navigation.navigate('NewQuerie', {
            paramKey: {
                id: route.params.Id
            }
        })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            updateQueries();
        });
        return unsubscribe;
    }, [navigation])

    return (
        <SafeAreaView style={styles.general}>
            <StatusBar animated={true} backgroundColor='#2B4257' />
            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                <Text style={styles.title}>Queries</Text>
                <TouchableOpacity onPress={() => createQuerie()} style={styles.createQuerie}><Text style={styles.createQuerieText}>Create a New Querie</Text></TouchableOpacity>
            </View>
            {isLoading ? <ActivityIndicator /> :
                <FlatList
                    data={queries}
                    keyExtractor={(item) => item._id}
                    renderItem={({ item }) =>
                        <Post title={item.title} description={item.description} onPress={() => seeComments(item._id)} />
                    }
                    refreshing={refresh}
                    onRefresh={() => updateQueries()}
                />
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    general: {
        backgroundColor: '#F2EBE9',
        flex: 1,
        marginHorizontal: 10,
    },
    title: {
        flex: 1,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
        color: '#2F5061',
    },
    post: {
        flex: 1,
        flexDirection: 'column',
        padding: 5,
        marginVertical: 8,
        borderRadius: 18,
        backgroundColor: '#14202E',
    },
    postTitle: {
        color: 'white',
        alignItems: 'flex-end',
        paddingHorizontal: 5,
        fontSize: 18,
        fontStyle: 'italic',
        fontWeight: 'bold',
    },
    postDescription: {
        color: 'white',
        fontSize: 13,
        paddingHorizontal: 5,
    },
    comments: {
        flex: 1,
        paddingHorizontal: 5,
    },
    commentsText: {
        color: '#E57F84',
        padding: 8,
    },
    reply: {
        flex: 0,
        paddingHorizontal: 5,
    },
    createQuerie: {
        flex: 0,
        padding: 7,
        margin: 3,
        backgroundColor: '#E57F84',
        borderRadius: 18,

    },
    createQuerieText: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#2F5061'
    },
    divider: {
        backgroundColor: 'white',
        margin: 3,
        height: 1,
    }

})


export default QueriesScreen;