import { StyleSheet, Text, ScrollView, StatusBar, Image, View, FlatList, TouchableOpacity, Linking, SafeAreaView, ActivityIndicator } from 'react-native';
import React, { useEffect, useState } from 'react';
import { Divider } from '@rneui/base';
import axios from 'axios';
import { ButtonGroup } from '@rneui/themed';

const NewsCard = ({ title, author, image, description, url }) => {
    return (
        <TouchableOpacity onPress={() => Linking.openURL(url)} >
            <View style={styles.new}>
                <Text style={styles.title}>{title}</Text>
                <Divider style={styles.divider} />
                <Text style={styles.author}>~{author}</Text>
                <Image style={styles.image} source={{ uri: image }} />
                <Text style={styles.description}>{description}</Text>
            </View>
        </TouchableOpacity >
    )
}
const NewsScreen = ({ navigation }) => {
    const [news, setNews] = useState([]);
    const [category, setCategory] = useState('General');
    const [isLoading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(false);


    const searchNewsByCategory = () => {
        setLoading(true);
        axios.get(`http://192.168.100.2:5000/new/${category}`)
            .then((res) => {
                setNews(res.data);
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            })
    }
    const GeneralButton = () => <TouchableOpacity onPress={() => {
        setCategory('General');
        setRefresh(!refresh);

    }} style={styles.newsButtons}><Text style={styles.newsButtonsText}>General</Text></TouchableOpacity>
    const TechButton = () => <TouchableOpacity onPress={() => {
        setCategory('Technology');
        setRefresh(!refresh);
    }} style={styles.newsButtons}><Text style={styles.newsButtonsText}>Tech</Text></TouchableOpacity>
    const BusinessButton = () => <TouchableOpacity onPress={() => {
        setCategory('Business')
        setRefresh(!refresh);
    }} style={styles.newsButtons}><Text style={styles.newsButtonsText}>Business</Text></TouchableOpacity>
    const HealthButton = () => <TouchableOpacity onPress={() => {
        setCategory('Health');
        setRefresh(!refresh);
    }} style={styles.newsButtons}><Text style={styles.newsButtonsText}>Health</Text></TouchableOpacity>
    const SportsButton = () => <TouchableOpacity onPress={() => {
        setCategory('Sports');
        setRefresh(!refresh);
    }} style={styles.newsButtons}><Text style={styles.newsButtonsText}>Sports</Text></TouchableOpacity>
    const ScienceButton = () => <TouchableOpacity onPress={() => {
        setCategory('Science');
        setRefresh(!refresh);
    }} style={styles.newsButtons}><Text style={styles.newsButtonsText}>Science</Text></TouchableOpacity>

    const GroupBoxButtons = [{ element: GeneralButton }, { element: TechButton }, { element: BusinessButton }]
    const GroupBoxButtons2 = [{ element: HealthButton }, { element: SportsButton }, { element: ScienceButton }]

    useEffect(() => {
        if (refresh || !refresh) {
            searchNewsByCategory();
        }
    }, [refresh])
    return (
        <SafeAreaView style={styles.general}>
            <StatusBar animated={true} backgroundColor='#2B4257' />
            <ButtonGroup buttons={GroupBoxButtons} containerStyle={styles.buttonGroupContainer} />
            <ButtonGroup buttons={GroupBoxButtons2} containerStyle={styles.buttonGroupContainer} />
            {isLoading ? <ActivityIndicator /> :
                <FlatList
                    data={news.data.news.articles}
                    renderItem={({ item }) =>
                        <NewsCard title={item.title} image={item.urlToImage} author={item.author} description={item.description} url={item.url} />
                    }
                    refreshing={refresh}
                />
            }
        </SafeAreaView>
    )
}

export default NewsScreen

const styles = StyleSheet.create({
    general: {
        backgroundColor: '#F2EBE9',
        flex: 1,
        paddingHorizontal: 8,
    },
    new: {
        margin: 5,
        borderRadius: 15,
        backgroundColor: '#14202E',
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 5,
        marginHorizontal: 10,
        color: 'white',
    },
    description: {
        fontSize: 13,
        margin: 10,
        color: 'white',
    },
    image: {
        height: 200,
        resizeMode: 'contain',
        marginHorizontal: 10,
        borderRadius: 10,
    },
    author: {
        fontStyle: 'italic',
        color: 'white',
        padding: 5,
        textAlign: 'right',
    },
    newsButtons: {
        width: '100%',
        height: '100%',
        padding: 7,
        alignItems: 'center'
    },
    buttonGroupContainer: {
        backgroundColor: '#2B4257',
        borderRadius: 20,
        height: 40,
    },
    newsButtonsText: {
        color: 'white',
    },
    divider: {
        backgroundColor: 'white',
        margin: 3,
        height: 1,
    }
})