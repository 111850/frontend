import { View, Text, ScrollView, StyleSheet, Image, StatusBar, Alert } from 'react-native'
import React, { useState } from 'react'

import CustomInput from '../components/CustomInput';
import CustomButton from '../components/CustomButton';
import axios from 'axios';

const SignUpScreen = ({ navigation }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [email, setEmail] = useState('');
    const [pin, setPin] = useState('');

    const onSignUpPressed = () => {
        if (username.length === 0 || password.length === 0 || passwordConfirm.length === 0 || email.length === 0 || pin === 0) {
            return (Alert.alert(
                "Error",
                "All fields must be provided",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        if (isNaN(parseInt(pin))) {
            return (Alert.alert(
                "Error",
                "The pin must be a number",
                [
                    {
                        text: "Cancel",
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true,
                }
            ))
        }
        axios.post('http://192.168.100.2:5000/user/signup', {
            user: username,
            password: password,
            passwordConfirm: passwordConfirm,
            email: email,
            pin: parseInt(pin)
        })
            .then((res) => {
                Alert.alert(
                    "There was an error",
                    "Verify your data and try logging in again",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
                setTimeout(navigation.navigate("Signin"), 5000);

            })
            .catch((err) => {
                Alert.alert(
                    "There was an error",
                    "Verify your data and try logging in again",
                    [
                        {
                            text: "Cancel",
                            style: "cancel",
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
            })
    }

    return (
        <ScrollView showsHorizontalScrollIndicator={false} >
            <View style={styles.root}>
                <StatusBar animated={true} backgroundColor='#2B4257' />
                <Image source={require('../../assets/images/PASSafeLogo.png')} />
                <Text style={styles.text}>Register</Text>
                <Text style={styles.text}>Please, fill the following fields with your information </Text>
                <CustomInput placeholder='User' value={username} setValue={setUsername} />
                <CustomInput placeholder='E-mail' value={email} setValue={setEmail} />
                <CustomInput placeholder='Password' value={password} setValue={setPassword} secureTextEntry />
                <CustomInput placeholder='Confirm Password' value={passwordConfirm} setValue={setPasswordConfirm} secureTextEntry />
                <CustomInput placeholder='PIN' keyboardType='numeric' value={pin} setValue={setPin} />

                <CustomButton text='Register' onPress={onSignUpPressed} />
                <CustomButton text='Already have an account? Sign in!' onPress={() => navigation.navigate("Signin")} type='TERTIARY' />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#F2EBE9'
    },
    text: {
        padding: 5,
        fontWeight: 'bold',
    }
})

export default SignUpScreen;