import { View, Text, ScrollView, StyleSheet, SectionList, TouchableOpacity, Linking, SafeAreaView, StatusBar } from 'react-native'
import React from 'react'

const DATA = [
    {
        title: "Programming Languages",
        data: [{ name: "JavaScript", url: "https://devdocs.io/javascript/" }, { name: "C++", url: "https://devdocs.io/cpp/" }, { name: "Python", url: "https://devdocs.io/python~3.10/" }]
    },
    {
        title: "IDEs",
        data: [{ name: "Visual Code", url: "https://code.visualstudio.com/download" }, { name: "NetBeans", url: "https://netbeans.apache.org/download/index.html" }, { name: "Atom", url: "https://atom.io" }]
    },
    {
        title: "Web Browsers",
        data: [{ name: "Opera GX", url: "https://www.opera.com/es/gx" }, { name: "Google Chrome", url: "https://www.google.com/intl/es-419/chrome/" }, { name: "Brave", url: "https://brave.com/es/download/" }]
    },
    {
        title: "Antivirus",
        data: [{ name: "MalwareBytes", url: "https://malwarebytes.com" }, { name: "Bitdefender", url: "https://www.bitdefender.es" }]
    },
    {
        title: "File Archiver",
        data: [{ name: "winRAR", url: "https://www.winrar.es/soporte/instalar/72/descargar-winrar-gratis" }, { name: "7zip", url: "https://www.7-zip.org/download.html" }, { name: "WinZip", url: "https://www.winzip.com/es/download/winzip/" }]
    },
    {
        title: "Repository Managment",
        data: [{ name: "Github", url: "https://github.com" }, { name: "Bitbucket", url: "https://bitbucket.org" }]
    },
    {
        title: "Music",
        data: [{ name: "Spotify", url: "https://www.spotify.com/ar/download/windows/" }, { name: "iTunes", url: "https://support.apple.com/es-es/HT210384" }, { name: "Deezer", url: "https://www.deezer.com/mx/download/" }]
    },
    {
        title: "Media",
        data: [{ name: "VLC", url: "https://www.videolan.org/vlc/download-windows.es.html" }]
    },
    {
        title: "Messaging",
        data: [{ name: "Whatsapp", url: "https://www.whatsapp.com/download//?l=uz&lang=es" }, { name: "Telegram", url: "https://web.telegram.org" }, { name: "Discord", url: "https://discord.com/download" }, { name: "Messenger", url: "https://www.messenger.com/desktop" }]
    },
    {
        title: "Video and Image Editors",
        data: [{ name: "Photoshop", url: "https://www.whatsapp.com/download//?l=uz&lang=es" }, { name: "GIMP", url: "https://www.gimp.org" }, { name: "DaVinci Resolve", url: "https://www.blackmagicdesign.com/products/davinciresolve" }, { name: "Adobe Premier Pro", url: "https://www.adobe.com/la/products/premiere/campaign/pricing.html" }]
    },
    {
        title: "File Hosting Services",
        data: [{ name: "DropBox", url: "https://www.dropbox.com/es/" }, { name: "Microsoft OneDrive", url: "https://onedrive.live.com/" }, { name: "Google Drive", url: "https://www.google.com/intl/es/drive/" }, { name: "MEGA", url: "https://mega.io" }]
    },
    {
        title: "Audio Editing",
        data: [{ name: "Audacity", url: "https://audacity.es" }]
    },
    {
        title: "Recording Software",
        data: [{ name: "OBS", url: "https://obsproject.com/es/download" }, { name: "StreamLabs", url: "https://streamlabs.com/streamlabs-live-streaming-software" }]
    },
    {
        title: "Notes",
        data: [{ name: "Notepad++", url: "https://notepad-plus-plus.org/downloads/" }, { name: "Sublime Text", url: "https://www.sublimetext.com" }]
    },
    {
        title: "Office",
        data: [{ name: "Libre Office", url: "https://es.libreoffice.org/descarga/libreoffice/" }, { name: "Microsoft Office", url: "https://www.office.com" }]
    },
    {
        title: "Entertainment",
        data: [{ name: "1v1.lol", url: "https://1v1.lol/es" }, { name: "Chess", url: "https://www.chess.com/home" }, { name: "QuakeJS", url: "http://www.quakejs.com/" }, { name: 'Mini Royale', url: "https://miniroyale2.io/" }]
    },
];

const Item = ({ title, url }) => (
    <View style={styles.item}>
        <TouchableOpacity onPress={() => Linking.openURL(url)}><Text style={styles.title}>{title}</Text></TouchableOpacity>
    </View >
);

const RecSoftwareScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.general}>
            <StatusBar animated={true} backgroundColor='#2B4257' />
            <Text style={styles.main}> Recommended Software </Text>
            <SectionList
                sections={DATA}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item }) => (
                    <Item title={`● ${item.name}`} url={item.url} />
                )}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={styles.header}>{title}</Text>
                )}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    general: {
        backgroundColor: '#F2EBE9',
        paddingBottom: 50,
    },
    header: {
        fontSize: 24,
        textAlign: 'center',
        backgroundColor: '#14202E',
        color: 'white',
        fontStyle: 'italic',
        borderRadius: 20,
        marginHorizontal: 50,
        marginVertical: 5,
    },
    item: {
        marginVertical: 4,
        marginHorizontal: 30,
    },
    title: {
        fontSize: 15,
        color: '#14202E',
    },
    main: {
        fontSize: 28,
        textAlign: 'center',
        backgroundColor: '#14202E',
        color: '#F4EAE6',
    },
    icon: {
        flexDirection: 'row',
    }
})

export default RecSoftwareScreen;