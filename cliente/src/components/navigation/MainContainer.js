import { StyleSheet, Text, View } from 'react-native'
import React, { useState, useEffect } from 'react'

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Ionicons from 'react-native-vector-icons/Ionicons';

import RecSoftwareScreen from '../../screens/RecSoftwareScreen';
import QueriesScreen from '../../screens/QueriesScreen';
import NewsScreen from '../../screens/NewsScreen';
import PasswordsScreen from '../../screens/PasswordsScreen';
import StatsScreen from '../../screens/StatsScreen';
// import CommentsScreen from '../../screens/CommentsScreen';
// import CreateQuerieScreen from '../../screens/CreateQuerieScreen';

const recSoftwareName = 'Software';
const queriesName = 'Queries';
const newsName = 'News';
const passwordsName = 'Passwords';
const statsName = "Stats";
// const commentsName = "Comments";
// const newQuerieName = "NewQuerie";

const Tab = createBottomTabNavigator();

const MainContainer = ({ route }) => {
    const userId = route.params.paramKey;
    // useEffect(() => {
    //     const userId = route.params.paramKey;
    //     console.log(userId);
    // }, [])

    return (
        <Tab.Navigator
            initialRouteName={passwordsName}
            screenOptions={({ route }) => ({
                "tabBarActiveTintColor": "#F4EAE6",
                "tabBarInactiveTintColor": "#F4EAE6",
                "tabBarLabelStyle": {
                    "paddingBottom": 10,
                    "fontSize": 10
                },
                "tabBarItemStyle": {
                    "height": 50,
                    "backgroundColor": "#2B4257"
                },
                "tabBarStyle": [
                    {
                        "display": "flex"
                    },
                    null
                ],
                headerShown: false,
                tabBarLabel: () => { return null },
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    let rn = route.name;

                    if (rn === passwordsName) {
                        iconName = focused ? 'lock-closed' : 'lock-closed-outline';
                    } else if (rn === queriesName) {
                        iconName = focused ? 'clipboard' : 'clipboard-outline';
                    } else if (rn === newsName) {
                        iconName = focused ? 'newspaper' : 'newspaper-outline';
                    } else if (rn === recSoftwareName) {
                        iconName = focused ? 'heart' : 'heart-outline';
                    } else if (rn === statsName) {
                        iconName = focused ? 'bar-chart' : 'bar-chart-outline'
                    }

                    return <Ionicons name={iconName} size={size} color={color} />;
                },
            })

            }
        >

            <Tab.Screen name={passwordsName} component={PasswordsScreen} initialParams={{ Id: userId }} />
            <Tab.Screen name={newsName} component={NewsScreen} initialParams={{ Id: userId }} />
            <Tab.Screen name={queriesName} component={QueriesScreen} initialParams={{ Id: userId }} />
            <Tab.Screen name={recSoftwareName} component={RecSoftwareScreen} initialParams={{ Id: userId }} />
            <Tab.Screen name={statsName} component={StatsScreen} initialParams={{ Id: userId }} />

            {/* <Tab.Screen name={commentsName} component={CommentsScreen} />
            <Tab.Screen name={newQuerieName} component={CreateQuerieScreen} /> */}


        </Tab.Navigator>
    )
}

export default MainContainer

const styles = StyleSheet.create({
    label: {
        padding: 10,
        fontSize: 10,
    },
    tabBar: {
        padding: 10,
        height: 70,
    }
})