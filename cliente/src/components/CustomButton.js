import { View, Text, Pressable, StyleSheet } from 'react-native'
import React from 'react'

const CustomButton = ({ onPress, text, type = 'PRIMARY' }) => {
    return (
        <Pressable onPress={onPress} style={[styles.container, styles[`container_${type}`]]}>
            <Text style={[styles.text, styles[`text_${type}`]]} >{text}</Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 5,
        alignItems: 'center',
        borderRadius: 5,
    },
    text: {
        fontWeight: 'bold',
        color: 'white',
    },
    container_PRIMARY: {
        backgroundColor: '#3b71f3',
        padding: 12,
    },
    container_TERTIARY: {
        padding: 6,
    },
    text_TERTIARY: {
        color: 'grey',
    }
})

export default CustomButton