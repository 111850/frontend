/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { SafeAreaView, StyleSheet, Text } from 'react-native';
import SignInScreen from './src/screens/SignInScreen';
import SignUpScreen from './src/screens/SignUpScreen';
import FreqAskedScreen from './src/screens/FreqAskedScreen';
import TermsAndConditionsScreen from './src/screens/TermsAndCondScreen';
import MainContainer from './src/components/navigation/MainContainer';

import CommentsScreen from './src/screens/CommentsScreen';
import CreateQuerieScreen from './src/screens/CreateQuerieScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { Provider } from 'react-redux';
// import store from './redux/store';
import reduxStore from './redux/store';

import { PersistGate } from 'redux-persist/integration/react';

const Stack = createNativeStackNavigator();

// instalar drawer y importar y inicializar

// const BottomTab = createBottomTabNavigator();

const App = () => {
  const { store, persistor } = reduxStore();
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor} >
        <NavigationContainer>
          <Stack.Navigator initialRouteName='Signin' screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Signin" component={SignInScreen} />
            <Stack.Screen name="Home" component={MainContainer} />
            <Stack.Screen name="Signup" component={SignUpScreen} />
            <Stack.Screen name="TermsOfService" component={TermsAndConditionsScreen} />
            <Stack.Screen name="FreqAsked" component={FreqAskedScreen} />
            <Stack.Screen name="Comments" component={CommentsScreen} />
            <Stack.Screen name="NewQuerie" component={CreateQuerieScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#F9FBFC',
  }
});

export default App;
