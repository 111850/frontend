import { configureStore } from "@reduxjs/toolkit";
import statsReducer from "./statsSlice";
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from "@react-native-async-storage/async-storage";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['navigation'],
}

const persistedReducer = persistReducer(persistConfig, statsReducer)

export default () => {
    let store = configureStore({
        reducer: { stats: persistedReducer },
        middleware: (getDefaultMiddleware) => getDefaultMiddleware({
            serializableCheck: false,
        })
    })
    // let store = createStore(persistedReducer)
    let persistor = persistStore(store)
    return { store, persistor }
}

// const customizedMiddleware = getDefaultMiddleware({
//     serializableCheck: false
// })

// export const store = configureStore({
//     reducer: { stats: statsReducer },
//     middleware: customizedMiddleware
// })