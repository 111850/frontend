import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    LongPasswordGenerated: 0,
    ShortPasswordGenerated: 0,
    PinPasswordGenerated: 0,
    GeneratedPassword: [], // dates
    CopiedPassword: [],
}

export const statsSlice = createSlice({
    name: 'stats',
    initialState,
    reducers: {
        // addCounterLongPassword: (state) => state.LongPasswordGenerated + 1,
        // addCounterShortPassword: (state) => state.ShortPasswordGenerated + 1,
        // addCounterPinPassword: (state) => state.PinPasswordGenerated + 1,
        // newPasswordGenerated: (state, payload) => state.GeneratedPassword.push(payload),
        // newPasswordCopied: (state, payload) => state.CopiedPassword.push(payload)

        addCounterLongPassword: (state) => {
            state.LongPasswordGenerated += 1
        },
        addCounterShortPassword: (state) => {
            state.ShortPasswordGenerated += 1
        },
        addCounterPinPassword: (state) => {
            state.PinPasswordGenerated += 1
        },
        newPasswordGenerated: (state) => {
            state.GeneratedPassword.push(new Date(Date.now()))
        },
        newPasswordCopied: (state) => {
            state.CopiedPassword.push(new Date(Date.now()))
        },
        addTests: (state) => {
            state.CopiedPassword.push(new Date("2022-05-13T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-05-16T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-05-19T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-05-20T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-05-05T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-06-05T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-06-04T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-06-03T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-06-02T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-20T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-13T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-10T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-11T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-14T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-05T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-02-02T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-09T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-16T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-17T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-28T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-25T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-02T18:26:38.516Z"))
            state.CopiedPassword.push(new Date("2022-01-01T18:26:38.516Z"))

            state.GeneratedPassword.push(new Date("2022-05-13T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-05-16T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-05-19T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-05-20T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-05-05T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-06-05T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-06-04T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-06-03T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-06-02T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-20T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-13T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-10T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-11T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-14T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-05T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-02-02T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-09T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-16T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-17T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-28T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-25T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-02T18:26:38.516Z"))
            state.GeneratedPassword.push(new Date("2022-01-01T18:26:38.516Z"))
        }

    },
})

export const {
    addCounterLongPassword,
    addCounterShortPassword,
    addCounterPinPassword,
    newPasswordGenerated,
    newPasswordCopied,
    addTests
} = statsSlice.actions;

export default statsSlice.reducer;